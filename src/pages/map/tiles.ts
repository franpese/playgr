export const pngtiles = {
    streetsv1: {name: 'streets-v1', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    streetsv2: {name: 'streets-v2', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.streets-basic/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},    
    light: {name: 'light', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.light/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    dark: {name: 'dark', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.dark/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    satellite: {name: 'satellite', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    hybrid: {name: 'hybrid', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.streets-satellite/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    wheatpaste: {name: 'wheatpaste', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.wheatpaste/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    comic: {name: 'comic', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.comic/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    outdoorsv1: {name: 'outdoors-v1', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.outdoors/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    outdoorsv2: {name: 'outdoors-v2', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.run-bike-hike/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    pencil: {name: 'pencil', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.pencil/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    pirates: {name: 'pirates', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.pirates/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    emerald: {name: 'emerald', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.emerald/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    contrast: {name: 'contrast', url: 'https://{s}.tiles.mapbox.com/v4/mapbox.high-contrast/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA'},
    cycle: {name: 'cycle', url: 'https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=699b576eea894e1fa097f241d6f93f5f'},
    transport: {name: 'transport', url: 'https://tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=699b576eea894e1fa097f241d6f93f5f'},
    transportdark: {name: 'transport-dark', url: 'https://tile.thunderforest.com/transport-dark/{z}/{x}/{y}.png?apikey=699b576eea894e1fa097f241d6f93f5f'},
    landscape: {name: 'landscape', url: 'https://tile.thunderforest.com/landscape/{z}/{x}/{y}.png?apikey=699b576eea894e1fa097f241d6f93f5f'},
    outdoorsv3: {name: 'outdoors-v3', url: 'https://tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=699b576eea894e1fa097f241d6f93f5f'},
    spinal: {name: 'spinal', url: 'https://tile.thunderforest.com/spinal-map/{z}/{x}/{y}.png?apikey=699b576eea894e1fa097f241d6f93f5f'},
    pioneer: {name: 'pioneer', url: 'https://tile.thunderforest.com/pioneer/{z}/{x}/{y}.png?apikey=699b576eea894e1fa097f241d6f93f5f'},
    atlas: {name: 'atlas', url: 'https://tile.thunderforest.com/mobile-atlas/{z}/{x}/{y}.png?apikey=699b576eea894e1fa097f241d6f93f5f'},
    neighbourhood: {name: 'neighbourhood', url: 'https://tile.thunderforest.com/neighbourhood/{z}/{x}/{y}.png?apikey=699b576eea894e1fa097f241d6f93f5f'}
}

export const svgtiles = {
    streets: {name: 'streets', url: 'mapbox://styles/mapbox/streets-v10'},
    outdoors: {name: 'outdoors', url: 'mapbox://styles/mapbox/outdoors-v10'},    
    light: {name: 'light', url: 'mapbox://styles/mapbox/light-v9'},
    dark: {name: 'dark', url: 'mapbox://styles/mapbox/dark-v9'},
    satellite: {name: 'satellite', url: 'mapbox://styles/mapbox/satellite-v9'},
    hybrid: {name: 'hybrid', url: 'mapbox://styles/mapbox/satellite-streets-v10'},
    navigationday: {name: 'navigation-day', url: 'mapbox://styles/mapbox/navigation-preview-day-v2'},
    navigationnight: {name: 'navigation-night', url: 'mapbox://styles/mapbox/navigation-preview-night-v2'},
    guidanceday: {name: 'guidance-day', url: 'mapbox://styles/mapbox/navigation-guidance-day-v2'},
    guidancenight: {name: 'guidance-night', url: 'mapbox://styles/mapbox/navigation-guidance-night-v2'}
}

export const mapboxapikey = "pk.eyJ1IjoiYWZwZXJlYSIsImEiOiJjamZkd3V5Mjgwc2E2MnFwZm9iMXdzYXVlIn0.4uV5ng4uXa9ifHG9ENT3FA";
export const thunderforestapikey = "699b576eea894e1fa097f241d6f93f5f";