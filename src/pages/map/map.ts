import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, PopoverController, Popover, Events } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { NgProgress } from 'ngx-progressbar';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { animInfoPanel } from '../../theme/animations';
import { MapMenuComponent } from '../../components/map-menu/map-menu';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { RestProvider } from '../../providers/rest/rest'; 
import { SearchParams, PlaceMarker, PlaceInfo } from '../../providers/rest/models';
import { pngtiles, svgtiles, mapboxapikey } from './tiles';
declare const L: any;
import 'leaflet';
import 'leaflet-routing-machine';
import 'mapbox-gl-leaflet';
import { latLng } from 'leaflet';
import { PlacePage } from '../place/place';



@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
  animations: [ animInfoPanel ]
})
export class MapPage {
 
  map: L.Map;
  routeMachine: any = null;
  tile: any;
  mapStyle = 9;
  searchParams: SearchParams;
  places: {id: number, name: string, type: number, evaluation: number, comments: number, marker: L.Marker}[] = []
  selectedPlace: {id: number, name: string, type: number, evaluation: number, comments: number, marker: L.Marker} = null
  placetypes: {name: string, id: number, show: boolean}[] = []
  markerGroups: L.LayerGroup[] = []
  icon = L.icon({
    iconUrl: 'assets/imgs/marker.svg',
    iconSize:     [50, 50],
    iconAnchor:   [25, 50]
  });
  meMarker: L.Marker = null
  meIcon: L.DivIcon = null

  constructor(private navCtrl: NavController, 
              private navParams: NavParams,
              private events: Events,
              private geolocation: Geolocation,
              private modalCtrl: ModalController,
              private popoverCtrl: PopoverController,
              private preferences: PreferencesProvider,
              private rest: RestProvider,
              private progress: NgProgress ) {
    this.configEvents()
  }

  configEvents(){
    this.events.subscribe('com.afperea.playgr.filterMarkers', () => { this.filterMarkers() })
  }

  leafletMap(){
    this.map = L.map('mapId', {
      center: [this.preferences.Settings.latitude,this.preferences.Settings.longitude],
      zoom: 15,
      minZoom: 6,
      maxZoom: 18,
      zoomControl: false,
      preferCanvas: true,
      attribution: "",
    })
    
    this.changeSvgTile()
  }

  changeSvgTile(){
    let tile = ""
    switch(this.mapStyle){
      case 0: tile = svgtiles.hybrid.url; this.mapStyle = 1; break
      case 1: tile = svgtiles.satellite.url; this.mapStyle = 2; break
      case 2: tile = svgtiles.navigationday.url; this.mapStyle = 3; break
      case 3: tile = svgtiles.guidanceday.url; this.mapStyle = 4; break
      case 4: tile = svgtiles.outdoors.url; this.mapStyle = 5; break
      case 5: tile = svgtiles.light.url; this.mapStyle = 6; break
      case 6: tile = svgtiles.dark.url; this.mapStyle = 7; break
      case 7: tile = svgtiles.navigationnight.url; this.mapStyle = 8; break
      case 8: tile = svgtiles.guidancenight.url; this.mapStyle = 9; break
      case 9: tile = svgtiles.streets.url; this.mapStyle = 0; break
    }
    if(this.tile != null){ this.map.removeLayer(this.tile) }
    this.tile = L.mapboxGL({ accessToken: mapboxapikey, style: tile })
    this.tile.addTo(this.map)
    
  }

  findPlaces(){
    this.progress.start()
    let bounds = this.map.getBounds()
    this.searchParams = new SearchParams()
    this.searchParams.latleft = bounds["_northEast"]["lat"]
    this.searchParams.lonleft = bounds["_southWest"]["lng"]
    this.searchParams.latright = bounds["_southWest"]["lat"]
    this.searchParams.lonright = bounds["_northEast"]["lng"]
    this.rest.findMarkers(this.searchParams)
    .subscribe( 
      places => { this.clearMarkers(); places.forEach( p => this.addMarker(p) ); this.filterPlaceTypes(); this.progress.done() }, 
      noplaces => { this.clearMarkers(); this.progress.done() }
    )
  }

  getPlaceInfo(marker: PlaceMarker){
    this.closeInfoPanel()
    setTimeout(() => {
      this.selectedPlace = this.places.filter( p => p.name === marker.name)[0]
    }, 200);
  }

  goToPlace(){
    let id = this.selectedPlace.id
    this.closeInfoPanel()
    setTimeout(() => {
      this.presentPlace(id)
    }, 200);
  }

  searchPlaces(params?: SearchParams){
    let bounds = this.map.getBounds()
    this.searchParams = new SearchParams()
    this.searchParams.latleft = bounds["_northEast"]["lat"]
    this.searchParams.lonleft = bounds["_southWest"]["lng"]
    this.searchParams.latright = bounds["_southWest"]["lat"]
    this.searchParams.lonright = bounds["_northEast"]["lng"]
    this.searchParams.potable_water = params.potable_water
    this.searchParams.toilets = params.toilets
    this.searchParams.restaurant = params.restaurant
    this.searchParams.cafe = params.cafe
    this.searchParams.surveillance = params.surveillance
    this.searchParams.pets_allowed = params.pets_allowed
    this.searchParams.wifi = params.wifi
  }

  clearMarkers(){ 
    this.selectedPlace = null
    this.places.forEach( place => this.map.removeLayer(place.marker))
    this.places = []
    this.placetypes = []
  }

  addMarker(marker: PlaceMarker){
    let placemarker: L.Marker = L.marker([marker.location.latitude, marker.location.longitude],{icon: this.icon}).on('click', m => { this.map.flyTo(m["latlng"],15); this.getPlaceInfo(marker) }).addTo(this.map)
    let place = {id: marker.nid, name: marker.name, type: marker.type.tid, evaluation: marker.evaluation, comments: marker.comments, marker: placemarker}
    this.places.push(place)
    this.placetypes.push( {name: marker.type.name, id: marker.type.tid, show: true} )
  }

  filterPlaceTypes(){ 
    this.placetypes = this.placetypes.filter((type, i, arr) => i === arr.findIndex((t) => ( t.id === type.id )) ) 
  }

  locate(){
    this.geolocation.getCurrentPosition()
    .then( location => { this.updateMyPosition(location) })
    .catch( error => console.log(error) )
  }

  setMeIcon(){
    if(this.preferences.session_stored && this.preferences.Session.user.picture["url"]){
      this.meIcon = L.divIcon({ 
        iconSize:     [50, 50],
        iconAnchor:   [25, 25],
        className: 'me-icon', 
        html: `<img src="${this.preferences.Session.user.picture["url"]}"/>`
      })
    }else{
      this.meIcon = L.divIcon({ 
        iconSize:     [50, 50],
        iconAnchor:   [25, 25],
        className: 'me-icon', 
        html: `<img src="assets/imgs/default_avatar.svg"/>`
      })
    }
  }

  updateMyPosition(position?: Geoposition){
    console.log("updateMyPositions")
    if(!this.meMarker){
      this.setMeIcon()
      this.meMarker = L.marker([position.coords.latitude,position.coords.longitude],{icon: this.meIcon}).addTo(this.map)
    }else{
      this.meMarker.setLatLng([position.coords.latitude,position.coords.longitude])
    }
    this.map.flyTo([position.coords.latitude,position.coords.longitude],15)
  }

  closeInfoPanel(){
    this.selectedPlace = null
  }

  filterMarkers(){
    this.placetypes.forEach( t => { 
      this.places.forEach( p => { 
        if(p.type == t.id){ 
          if(t.show){ p.marker.addTo(this.map) }
          else{ this.map.removeLayer(p.marker) } 
        } 
      }) 
    })
  }

  calculateRouteTo(lat: number, lon: number){
    if(this.routeMachine != null){ this.map.removeControl(this.routeMachine) }
    this.geolocation.getCurrentPosition()
    .then( location => {
      this.routeMachine = L.Routing.control({
        waypoints: [
          L.latLng(location.coords.latitude, location.coords.longitude),
          L.latLng(lat, lon)
        ],
        router: L.Routing.mapbox(mapboxapikey)
      })
      this.routeMachine.addTo(this.map);
    })
    .catch( error => console.log(error) )
  }

  presentMapMenu(myEvent) {
    this.closeInfoPanel()
    let popover = this.popoverCtrl.create(MapMenuComponent,this.placetypes,{ cssClass: null, showBackdrop: true, enableBackdropDismiss: true })
    popover.present({ ev: myEvent })
  }

  presentPlace(id: number){
    this.closeInfoPanel()
    let modal = this.modalCtrl.create(PlacePage,{id},{ cssClass: null, showBackdrop: true, enableBackdropDismiss: true })
    modal.onDidDismiss( to => { if(to){ this.calculateRouteTo(to["latitude"], to["longitude"]) } })
    setTimeout(() => {
      modal.present()
    }, 200);
  }

  ionViewDidLoad() {
    this.leafletMap();
  }

}
