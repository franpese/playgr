import { Component } from '@angular/core';
import { AppState } from '../../app/app.global';
import { ViewController, IonicPage, Events, NavParams } from 'ionic-angular';
import { NgProgress } from 'ngx-progressbar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { RestProvider } from '../../providers/rest/rest';
import { UserSignup } from '../../providers/rest/models';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  private signupForm: FormGroup
  private signup: UserSignup = new UserSignup()

  constructor(private global: AppState,
              private viewCtrl: ViewController,
              private params: NavParams,
              private events: Events,
              private translate: TranslateService,
              private formBuilder: FormBuilder,
              private preferences: PreferencesProvider,
              private rest: RestProvider,
              private progress: NgProgress) {
  this.signup.name = this.params["data"]["value"]["username"]
  this.signup.pass = this.params["data"]["value"]["password"]
  this.signupForm = this.createLoginForm()
}

  private createLoginForm(){
    return this.formBuilder.group({
      name: [this.signup.name, [Validators.required]],
      mail: [this.signup.mail, [Validators.required]],
      pass: [this.signup.pass, [Validators.required]]
    });
  }

  doSignup(){
    this.progress.start()
    this.rest.signup(this.signupForm.value).toPromise()
    .then( () => { this.progress.done(); this.viewCtrl.dismiss(this.signupForm.value) })
    .catch( err => { this.progress.done(); console.log(err) })
  }

  goBack(){ this.viewCtrl.dismiss() }

}
