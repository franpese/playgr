import { Component } from '@angular/core';
import { AppState } from '../../app/app.global';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';
import { NgProgress } from 'ngx-progressbar';
import { TranslateService } from '@ngx-translate/core';
import { RestProvider } from '../../providers/rest/rest';
import { PlaceInfo, PlaceComment, PlaceAdress } from '../../providers/rest/models';



@IonicPage()
@Component({
  selector: 'page-place',
  templateUrl: 'place.html'
})
export class PlacePage {

  section = "description";
  info: PlaceInfo = null;
  address = new PlaceAdress();
  comments: PlaceComment[] = null;

  showingRoute: boolean = false;

  constructor(public global: AppState,
    private viewCtrl: ViewController,
    private params: NavParams,
    private rest: RestProvider,
    private progress: NgProgress,
    private translate: TranslateService) {
    this.getPlaceInfo(this.params.data["id"])
    this.getPlaceComments(this.params.data["id"])
  }

  getPlaceInfo(id: number) {
    this.progress.start()
    this.rest.getPlaceInfo(id)
      .subscribe(
        infodata => {
          //setTimeout(() => {
          this.info = infodata[0]
          this.info.type.name = this.translate.instant("Type."+this.info.type.tid)
          this.info.ownership.name = this.translate.instant("Ownership."+this.info.ownership.tid)
          this.info.environment.name = this.translate.instant("Environment."+this.info.environment.tid)
          this.geocodeLocation(this.info.location.latitude, this.info.location.longitude)
          //}, 100000);
        },
        noinfo => { this.progress.done(); console.log(noinfo) }
      )
  }

  getPlaceComments(id: number) {
    this.rest.getPlaceComments(id)
      .subscribe(
        commentsdata => {
          this.comments = commentsdata
        },
        nocomments => { console.log(nocomments) }
      )
  }

  geocodeLocation(lat: number, lon: number) {
    this.rest.getAddress(lat, lon)
      .subscribe(
        location => {

          this.address.country = location[location.length - 1]
          location.pop()
          this.address.community = location[location.length - 1]
          location.pop()
          this.address.province = location[location.length - 1]
          location.pop()
          this.address.city = location[location.length - 1]
          location.pop()
          location.pop()
          this.address.description = location.join(',')

          this.progress.done()
        },
        nolocation => { this.progress.done(); console.log(nolocation) }
      )
  }

  calculateRoute() {
    this.viewCtrl.dismiss({ to: this.info.location })
  }

  dismiss() { this.viewCtrl.dismiss() }

}
