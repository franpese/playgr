import { Component } from '@angular/core';
import { AppState } from '../../app/app.global';
import { ViewController, IonicPage, Events, ModalController, AlertController } from 'ionic-angular';
import { NgProgress } from 'ngx-progressbar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { RestProvider } from '../../providers/rest/rest';
import { UserLogin } from '../../providers/rest/models';
import { Session } from '../../providers/rest/interfaces';
import { SignupPage } from '../signup/signup';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private loginForm: FormGroup
  private login: UserLogin = new UserLogin()

  constructor(private global: AppState,
              private viewCtrl: ViewController,
              private modalCtrl: ModalController,
              private alertCtrl: AlertController,
              private events: Events,
              private translate: TranslateService,
              private formBuilder: FormBuilder,
              private preferences: PreferencesProvider,
              private rest: RestProvider,
              private progress: NgProgress) {
    this.loginForm = this.createLoginForm()
  }

  private createLoginForm(){
    return this.formBuilder.group({
      username: [this.login.name, [Validators.required]],
      password: [this.login.password, [Validators.required]]
    });
  }

  doLogin(){
    this.progress.start()
    this.events.publish('com.afperea.playgr.isLogin', true)
    this.rest.getTemporalToken().toPromise()
    .then( temporalToken =>
      this.rest.login( temporalToken, this.loginForm.value ).toPromise()
      .then((newSession: Session) => {
        console.log(newSession)
        this.preferences.saveSession(newSession)
        .then( () => {
          this.progress.done()
          this.events.publish('com.afperea.playgr.isLogin', false)
          this.viewCtrl.dismiss()
        }).catch( error => { this.progress.done(); console.log(error) })
      }).catch( error => { this.progress.done(); console.log(error) })
    ).catch( error => { this.progress.done(); console.log(error) })
  }

  signup(){
    let signupModal = this.modalCtrl.create(SignupPage, { value: this.loginForm.value }, { cssClass: 'inset-modal-signup' })
    signupModal.onDidDismiss( params => { 
      if(params){
        this.login.name = params["name"]
        this.login.password = params["pass"]
        this.loginForm = this.createLoginForm()
        this.showAccountActivationAlert()
      }
    })
    signupModal.present()
  }

  private showAccountActivationAlert(){
    let info = this.alertCtrl.create({
      title: this.translate.instant('Page.Logn.alert_title'),
      subTitle: this.translate.instant('Page.Logn.alert_subtitle'),
      buttons: [this.translate.instant('Page.Logn.alert_button')]
    })
    info.present()
  }

  goBack(){ this.viewCtrl.dismiss() }

}
