import { Injectable } from '@angular/core';


@Injectable()
export class AppState {
  _state = {};

  // Getter del objeto _state (Devuelve un objeto clonado del _state original para no manipular directamente esta propiedad del sistema).
  get state(){ return this._state = this.clone(this._state); }
  
  // Devuelve una instancia de _state clonada para no manipular directamente esta propiedad del sistema.
  get(prop?: any){ const state = this.state; return state.hasOwnProperty(prop) ? state[prop] : state; }
  
  //Método clonador de la instancia de _state (estado de la aplicación). 
  private clone(object){ return JSON.parse(JSON.stringify(object)); }
  
  // Asigna un tema a la propiedad del sistema 'theme'
  set(prop: string, value: any){ return this._state[prop] = value; }
}