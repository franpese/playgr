import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Ionic2RatingModule } from 'ionic2-rating';
import { NgProgressModule } from 'ngx-progressbar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { FindPage } from '../pages/find/find';
import { FavouritesPage } from '../pages/favourites/favourites';
import { SettingsPage } from '../pages/settings/settings';
import { AboutPage } from '../pages/about/about';

import { AppState } from './app.global';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SQLite } from '@ionic-native/sqlite';
import { IonicStorageModule } from '@ionic/storage';

import { StorageProvider } from '../providers/storage/storage';
import { PreferencesProvider } from '../providers/preferences/preferences';
import { RestProvider } from '../providers/rest/rest';

import { MapMenuComponent } from '../components/map-menu/map-menu';
import { CommentsSliderComponent } from '../components/comments-slider/comments-slider';
import { MapRoueCalculatorComponent } from '../components/map-roue-calculator/map-roue-calculator';
import { PlacePage } from '../pages/place/place';

export function HttpLoaderFactory(_http: HttpClient) {
  return new TranslateHttpLoader(_http, "./assets/i18n/", ".json");
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignupPage,
    LoginPage,
    MapPage,
    FindPage,
    FavouritesPage,
    SettingsPage,
    AboutPage,
    MapMenuComponent,
    CommentsSliderComponent,
    MapRoueCalculatorComponent,
    PlacePage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    IonicStorageModule.forRoot({ name: 'playgr' }),
    Ionic2RatingModule,
    NgProgressModule
  ],
  bootstrap: [IonicApp],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  entryComponents: [
    MyApp,
    HomePage,
    SignupPage,
    LoginPage,
    MapPage,
    FindPage,
    FavouritesPage,
    SettingsPage,
    AboutPage,
    MapMenuComponent,
    CommentsSliderComponent,
    MapRoueCalculatorComponent,
    PlacePage
  ],
  providers: [
    AppState,
    StatusBar,
    SplashScreen,
    SQLite,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    StorageProvider,
    Geolocation,
    PreferencesProvider,
    RestProvider
  ]
})
export class AppModule {}
