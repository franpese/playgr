import { Component, ViewChild } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Nav, Platform, MenuController, Events, ModalController } from 'ionic-angular';
import { TranslateService } from "@ngx-translate/core";
import { AppState } from './app.global';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { MapPage } from '../pages/map/map';
import { FindPage } from '../pages/find/find';
import { FavouritesPage } from '../pages/favourites/favourites';
import { SettingsPage } from '../pages/settings/settings';
import { AboutPage } from '../pages/about/about';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';

import { PreferencesProvider } from '../providers/preferences/preferences'
import { StorageProvider } from '../providers/storage/storage'
import { RestProvider } from '../providers/rest/rest'

@Component({
  templateUrl: 'app.html',
  animations: [ ]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  isLogin: Boolean = false;
  isLogout: Boolean = false;

  rootPage: any = HomePage;
  user: {avatar: string, username: any, mail: string} = null;
  pages: Array<{title: string, component: any, icon: string}>;

  constructor(private global: AppState,
              private platform: Platform, 
              private events: Events,
              private translate: TranslateService,
              private statusBar: StatusBar, 
              private splashScreen: SplashScreen,
              private menuCtrl: MenuController,
              private modalCtrl: ModalController,
              private preferences: PreferencesProvider,
              private storage: StorageProvider,
              private rest: RestProvider) {
    this.initializeApp()
  }

  initializeApp() {
    this.platform.ready()
    .then(() => this.subscribeEvents() )
    .catch( err => console.log(err) )
  }

  subscribeEvents(){
    this.events.subscribe("com.afperea.playgr.preferences.ready", () => {
      this.global.set('theme', this.preferences.Settings.theme)
      this.statusBar.backgroundColorByName(this.preferences.Settings.status_bar)
      this.translate.setDefaultLang(this.preferences.Settings.language)
      this.pages = [
        { title: 'Page.Home.title', component: HomePage, icon: 'home' },
        { title: 'Page.Maps.title', component: MapPage, icon: 'map' },
        { title: 'Page.Find.title', component: FindPage, icon: 'search' },
        { title: 'Page.Favs.title', component: FavouritesPage, icon: 'star' },
        { title: 'Page.Stng.title', component: SettingsPage, icon: 'settings' },
        { title: 'Page.Abot.title', component: AboutPage, icon: 'information-circle' }
      ]
      this.nav.setRoot(MapPage);
      this.splashScreen.hide()
    })
    this.events.subscribe("com.afperea.playgr.session.ready", () => {
      if(this.preferences.session_stored){ console.log(this.preferences.Session); this.openSession() }
    })
    this.events.subscribe("com.afperea.playgr.isLogin", login => {
      this.isLogin = login
      if(!this.isLogin){ this.openSession() }
    })
  }

  openSession(){
    this.user = { 
      username: this.preferences.Session.user.name,
      mail: this.preferences.Session.user.mail[0],
      avatar: this.preferences.Session.user.picture["url"]
    }
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  login(){ this.modalCtrl.create(LoginPage, null, { cssClass: 'inset-modal-login' }).present() }

  logout(){
    this.isLogout = true
    this.rest.logout().toPromise()
    .then( response => {
      if(response[0] == true){ 
        this.preferences.clearSession().then(() => { 
          if( this.nav.getViews().length > 1 ){ this.nav.popToRoot() }
          this.user = null
          this.isLogout = false
        }).catch( error => { this.isLogout = false; console.log(error) } )
      }else{ this.isLogout = false; console.log(response) }
    }).catch( error => { this.isLogout = false; console.log(error) } )
  }

}
