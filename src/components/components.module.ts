import { NgModule } from '@angular/core';
import { MapMenuComponent } from './map-menu/map-menu';
import { CommentsSliderComponent } from './comments-slider/comments-slider';
import { MapRoueCalculatorComponent } from './map-roue-calculator/map-roue-calculator';

@NgModule({
	declarations: [MapMenuComponent,
    CommentsSliderComponent,
    MapRoueCalculatorComponent],
	imports: [],
	exports: [MapMenuComponent,
    CommentsSliderComponent,
    MapRoueCalculatorComponent]
})
export class ComponentsModule {}
