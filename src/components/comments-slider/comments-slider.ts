import { Component } from '@angular/core';

@Component({
  selector: 'comments-slider',
  templateUrl: 'comments-slider.html'
})
export class CommentsSliderComponent {

  text: string;

  constructor() {
    this.text = 'Hello World';
  }

}
