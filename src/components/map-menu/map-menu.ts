import { Component } from '@angular/core';
import { AppState } from '../../app/app.global';
import { ViewController, NavParams, Events } from 'ionic-angular';


@Component({
    selector: 'map-menu',
    templateUrl: 'map-menu.html',
})

export class MapMenuComponent {

    private placetypes: any[] = []
  
    constructor(private global: AppState,
                private viewCtrl: ViewController,
                private events: Events,
                private params: NavParams) {
        this.placetypes = this.params["data"]
  }

  valueChange(){
    this.events.publish('com.afperea.playgr.filterMarkers')
  }

}