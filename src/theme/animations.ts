import { trigger, state, transition, style, animate } from '@angular/animations';

export const animInfoPanel = trigger('animInfoPanel', 
[   
    transition(':enter', [ 
        style({transform: 'translateY(100%)'}), 
        animate(100, style({transform: 'translateY(0)'})) 
    ]),
    transition(':leave', [
        animate(100, style({transform: 'translateY(100%)'})) 
    ])
]
);

export const ghosting = trigger('ghosting', 
[   
    transition(':enter', [ 
        style({color: 'rgba(0,0,0,.15)'}), 
        animate(2000, style({color: 'rgba(0,0,0,.1)'})) 
    ]),
    transition(':leave', [
        animate(2000, style({color: 'rgba(0,0,0,.15)'})) 
    ])
]
);