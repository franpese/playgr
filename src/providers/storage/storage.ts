import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PreferencesProvider } from '../preferences/preferences';
import { Place } from '../rest/interfaces';


@Injectable()
export class StorageProvider {

  TAG = "[Database]--> "
  private dbReady = new BehaviorSubject<boolean>(false);
  private database: SQLiteObject;

  constructor(private platform: Platform,
              private preferences: PreferencesProvider,
              private sqlite: SQLite) {
    this.openDB()
  }

  
  private isReady(){
    return new Promise( (resolve, reject) => {
      if(this.dbReady.getValue()){ resolve() }
      else{ this.dbReady.subscribe( ready => { if(ready){ resolve() } } ) }  
    })
  }
  private openDB(){
    this.platform.ready().then( () => {
      this.sqlite.create({ name: 'user_storage.db', location: 'default'})
      .then( db => {
        this.database = db
        this.createTables()
        .then( () => this.dbReady.next(true) )
        .catch( err => console.log(this.TAG,err) )
      })
      .catch( err => console.log(this.TAG,err) )        
    })
    .catch( err => console.log(this.TAG,err) )
  }
  private createTables(){ return this.database.sqlBatch([this.preferences.DBConfig.create]) }


  public getExist(id: number){
    return this.isReady().then( () => {
      return this.database.executeSql(this.preferences.DBConfig.exist, [id])
      .then( data => { 
        let response = []
        for(let i=0; i<data.rows.length; i++){ response.push(data.rows.item(i)) }
        return response
      })
      .catch( err => console.log(this.TAG,err) )
    })
    .catch( err => console.log(this.TAG,err) )
  }
  

  public addFavourite(fav: Place){
    return this.isReady().then( () => {
      return this.database.executeSql( this.preferences.DBConfig.insert, [fav.id, fav.name, fav.type_id, fav.loc_latitude, fav.loc_longitude] )
      .catch( err => console.log(this.TAG,err) )
    })
    .catch( err => console.log(this.TAG,err) )
  }
  public getFavourites(){
    return this.isReady().then( () => {
      return this.database.executeSql(this.preferences.DBConfig.select, [])
      .then( data => {
        let favourites = []
        for(let i=0; i<data.rows.length; i++){ favourites.push(data.rows.item(i)) }
        return favourites
      })
      .catch( err => console.log(this.TAG,err) )
    })
    .catch( err => console.log(this.TAG,err) )
  }
  public removeFavourite(id: number){
    return this.isReady().then( () => {
      return this.database.executeSql( this.preferences.DBConfig.delete, [id])
      .catch( err => console.log(this.TAG,err) )
    })
    .catch( err => console.log(this.TAG,err) )
  }

  
}
