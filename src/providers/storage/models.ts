export class Favourite{
  
    constructor(public id: number, public name: string, public type: number, public latitude: number, public longitude: number){ 
    }
  
    static fromJson(data:any){
      if(!data.id || !data.name || !data.type || !data.latitude || !data.longitude){
        throw(new Error("Invalid argument: Unknown data structure."));
      }else{
        return new Favourite(data.id, data.name, data.type, data.latitude, data.longitude);
      }
    }
    
}