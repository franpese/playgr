import { Platform, Events } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ApiConfig, SettingsConfig, DbConfig } from './interfaces';
import { Session } from '../rest/interfaces';


@Injectable()
export class PreferencesProvider {

  TAG = "[Preferences]--> "
  private settings: SettingsConfig;
  private apiConfig: ApiConfig;
  private dbConfig: DbConfig;
  private SETTINGS_KEY: string = 'com.afperea.playgr.settings';
  private SESSION_KEY: string = 'com.afperea.playgr.session';
  private session: Session;
  public session_stored: boolean;

  constructor(private platform: Platform, 
              private events: Events,
              private http: HttpClient, 
              private storage: Storage) {
    this.init()
  }


  get Settings(): SettingsConfig{ return this.settings }
  get ApiConfig(): ApiConfig{ return this.apiConfig }
  get DBConfig(): DbConfig{ return this.dbConfig }
  get Session(): Session{ return this.session }


  private init(){
    this.platform.ready().then( () => {
      Promise.all([ this.loadSettings(), this.loadApiConfig(), this.loadDBConfig() ])
      .then(() => this.loadSession())
      .then(() => this.events.publish('com.afperea.playgr.preferences.ready'))
      .catch( err => console.log(this.TAG,err) )
    })
    .catch( err => console.log(this.TAG,err) )
  }


  private loadSettings(): Promise<any>{
    return this.storage.get(this.SETTINGS_KEY)
    .then( value => { if (value){  return this.settings = value; } else{ return this.loadDefaultSettings() }})
    .catch( err => console.log(this.TAG,err) )
  }
  private loadDefaultSettings(): Promise<any>{
    return this.http.get<SettingsConfig>("assets/config/settings.json").toPromise()
    .then( value => { this.settings = value; return this.setSettings(this.settings) })
    .catch( err => console.log(this.TAG,err) )
  }
  private setSettings(value: SettingsConfig): Promise<any> {
    return this.storage.set(this.SETTINGS_KEY, value)
  }


  private loadApiConfig(): Promise<any>{
    return this.http.get<ApiConfig>("assets/config/api.json").toPromise()
    .then( value => this.apiConfig = value )
    .catch( err => console.log(this.TAG,err) )
  }


  private loadDBConfig(): Promise<any>{
    return this.http.get<DbConfig>("assets/config/db.json").toPromise()
    .then( value => this.dbConfig = value )
    .catch( err => console.log(this.TAG,err) )
  }


  public loadSession(): Promise<any>{
    return this.storage.get(this.SESSION_KEY)
    .then( value => { if(value){ this.session = value; this.session_stored = true }else{ this.session_stored = false } })
    .then(() => this.events.publish('com.afperea.playgr.session.ready'))
    .catch( err => console.log(this.TAG,err) )
  }
  public saveSession(value: Session): Promise<any> {
    this.session = value;
    this.session_stored = true;
    return this.savePeferences();
  }
  public clearSession(): Promise<any>{
    return this.storage.remove(this.SESSION_KEY)
    .then( () => { this.session_stored = false; this.session = null })
    .catch( err => console.log(this.TAG,err) )
  }
  private setSession(value: Session): Promise<any> {
    return this.storage.set(this.SESSION_KEY, value);
  }

  public savePeferences() {
    return Promise.all([ this.setSettings(this.settings), this.setSession(this.session) ])
    .catch( err => console.log(this.TAG,err) )
  }
}
