export interface ApiConfig {
    url: string,
    key : string,
    token : string,
    temp_token : string,
    signup : string,
    login : string,
    logout : string,
    place : string,
    comments : string,
    type : string,
    environment : string,
    ownership : string,
    markers : string,
    geocoding : string
}
export interface DbConfig {
    create : string,
    insert : string,
    select : string,
    delete : string,
    exist : string
}
export interface SettingsConfig {
    language : string,
    theme : string,
    status_bar : string,
    latitude : number,
    longitude : number,
    session_token : string,
    uid : number,
    username : string,
    password : string
}