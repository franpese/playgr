export interface Session{
    sessid: string[];
    session_name: string[];
    token: string[];
    user: {
        uid: string[];
        name: string[];
        mail: string[];
        theme: string;
        signature: string;
        signature_format: string;
        created: string;
        access: string;
        login: string;
        status: string;
        timezone: string;
        language: string;
        picture: string;
        data: string;
        roles: string;
        rdf_mapping: string;
    }
}

export interface Place {
    id: number;
    name: string;
    description: string;
    type_id: number;
    images: any[];
    creation: string;
    modification: string;
    author: string;
    evaluation: number;
    minimum_age: number;
    maximum_age: number;
    ownership: number;
    environment: number;
    adult_rates: number;
    child_rates: number;
    capacity: number;
    loc_latitude: number;
    loc_longitude: number;
    potable_water: any;
    toilets: any;
    restaurant: any;
    cafe: any;
    surveillance: any;
    pets_allowed: any;
    wifi: any;
}