export class UserLogin {
    public name : string = "";
    public password : string = "";
}

export class UserSignup {
    public name : string = "";
    public pass : string = "";
    public mail : string = "";
}

export class SearchParams{
    public latleft: number;
    public lonleft: number;
    public latright: number;
    public lonright: number;
    public potable_water: boolean;
    public toilets: boolean;
    public restaurant: boolean;
    public cafe: boolean;
    public surveillance: boolean;
    public pets_allowed: boolean;
    public wifi: boolean;

    constructor(){}

    public toUrlParams(): String{
        let urlParams = `latleft=${this.latleft}&lonleft=${this.lonleft}&latright=${this.latright}&lonright=${this.lonright}`
        if(this.potable_water != null){ urlParams.concat(`&potable_water=${(this.potable_water) ? 1 : 0 }`) }
        if(this.toilets != null){ urlParams.concat(`&toilets=${(this.toilets) ? 1 : 0 }`) }
        if(this.restaurant != null){ urlParams.concat(`&restaurant=${(this.restaurant) ? 1 : 0 }`) }
        if(this.cafe != null){ urlParams.concat(`&cafe=${(this.cafe) ? 1 : 0 }`) }
        if(this.surveillance != null){ urlParams.concat(`&surveillance=${(this.surveillance) ? 1 : 0 }`) }
        if(this.pets_allowed != null){ urlParams.concat(`&pets_allowed=${(this.pets_allowed) ? 1 : 0 }`) }
        if(this.wifi != null){ urlParams.concat(`&wifi=${(this.wifi) ? 1 : 0 }`) }
        return urlParams
    }
}

export class PlaceMarker{
    public nid: number;
    public name: string;
    public evaluation: number;
    public comments: number;
    public type: PlaceType;
    public location: PlaceMarkerLocation;
    
    constructor(values: Object = {}){ Object.assign(this, values) }
}

export class PlaceMarkerLocation{
    public latitude: number; 
    public longitude: number;
    
    constructor(values: Object = {}){ Object.assign(this, values) }
}

export class PlaceInfo{
    public nid: number;
    public author: Author;
    public creation: string;
    public modification: string;
    public name: string;
    public description: string;
    public type: PlaceType;
    public location: PlaceInfoLocation;
    public images: [PlaceInfoImage];
    public evaluation: number;
    public comments: number;
    public environment: PlaceInfoEnvironment;
    public minimum_age: number;
    public maximum_age: number;
    public capacity: number;
    public ownership: PlaceInfoOwnership;
    public child_rates: number;
    public child_adults: number;
    public potable_water: boolean;
    public toilets: boolean;
    public cafe: boolean;
    public restaurant: boolean;
    public wifi: boolean;
    public pets_allowed: boolean;
    public surveillance: boolean;

    constructor(values: Object = {}){ Object.assign(this, values) }
}

export class Author{
    public uid: number; 
    public name: string;

    constructor(values: Object = {}){ Object.assign(this, values) }
}

export class PlaceType{
    public tid: number; 
    public name: string;

    constructor(values: Object = {}){ Object.assign(this, values) }
}

export class PlaceInfoLocation{
    public latitude: number; 
    public longitude: number;

    constructor(values: Object = {}){ Object.assign(this, values) }
}

export class PlaceInfoImage{
    public fid: number; 
    public url: string;

    constructor(values: Object = {}){ Object.assign(this, values) }
}

export class PlaceInfoEnvironment{
    public tid: number; 
    public name: string;

    constructor(values: Object = {}){ Object.assign(this, values) }
}

export class PlaceInfoOwnership{
    public tid: number; 
    public name: string;

    constructor(values: Object = {}){ Object.assign(this, values) }
}

export class PlaceComment{
    public author: Author;
    public creation: string;
    public description: string;
    public evaluation: number;
    
    constructor(values: Object = {}){ Object.assign(this, values) }
}

export class PlaceAdress{
    public description: string = "";
    public city: string = "";
    public province: string = "";
    public community: string = "";
    public country: string = "";
    
    constructor(){}
}