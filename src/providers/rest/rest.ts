import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { PreferencesProvider } from '../preferences/preferences';
import { UserSignup, UserLogin, SearchParams, PlaceMarker, PlaceInfo, PlaceComment } from './models';

@Injectable()
export class RestProvider {

  TAG = "[API]--> "
  httpOptions = {};

  constructor(private http: HttpClient,
              private preferences: PreferencesProvider ) {
  }

  
  private configDefaultHeaders(){
    this.httpOptions = { 
      headers: new HttpHeaders({ 
        'token': this.preferences.ApiConfig.token,
        'API-KEY': this.preferences.ApiConfig.key 
      })
    }
  }
  private configTempTokenHeaders( token: any ){
    this.httpOptions = { 
      headers: new HttpHeaders({ 
        'TOKEN' : token 
      }) 
    }
  }
  private configSessionHeaders(){ 
    this.httpOptions = { headers: new HttpHeaders(
      { 
      'X-CSRF-Token' : this.preferences.Session.token,
      'cookie'  : this.preferences.Session.session_name+"="+this.preferences.Session.sessid
      }
    )}
  }


  public signup( accountInfo: UserSignup ): Observable<any>{
    this.configDefaultHeaders()
    return this.http.post(this.preferences.ApiConfig.url+this.preferences.ApiConfig.signup, accountInfo, this.httpOptions).share()
  }


  public getTemporalToken(): Observable<any>{
    return this.http.post(this.preferences.ApiConfig.url+this.preferences.ApiConfig.temp_token, {}).share()
  }


  public login( temporalToken: any, userLogin: UserLogin ): Observable<any>{
    this.configTempTokenHeaders({token: [temporalToken]})
    return this.http.post(this.preferences.ApiConfig.url+this.preferences.ApiConfig.login, userLogin, this.httpOptions).share()
  }


  public logout(): Observable<any>{
    this.configSessionHeaders()
    return this.http.post( this.preferences.ApiConfig.url+this.preferences.ApiConfig.logout, {}, this.httpOptions ).share()
  }

  public findMarkers(params: SearchParams): Observable<PlaceMarker[]> {
    this.configDefaultHeaders()
    return this.http.get( this.preferences.ApiConfig.url+this.preferences.ApiConfig.markers+params.toUrlParams(), this.httpOptions)
    .map( response => { 
      const places = JSON.parse(JSON.stringify(response))
      return places.map( place => new PlaceMarker(place) ) 
    })
  }

  public getPlaceInfo(id: number): Observable<PlaceInfo> {
    this.configDefaultHeaders()
    return this.http.get( this.preferences.ApiConfig.url+this.preferences.ApiConfig.place+id, this.httpOptions)
    .map( response => { 
      const place = JSON.parse(JSON.stringify(response))
      return new PlaceInfo(place)
    })
  }

  public getPlaceComments(id: number): Observable<PlaceComment[]> {
    this.configDefaultHeaders()
    return this.http.get( this.preferences.ApiConfig.url+this.preferences.ApiConfig.comments+id, this.httpOptions)
    .map( response => { 
      const comments = JSON.parse(JSON.stringify(response))
      let commentsarray: PlaceComment[] = []
      comments.forEach(comment => commentsarray.push(new PlaceComment(comment))) 
      return commentsarray
    })
  }

  public getAddress(lat: number, lon: number): Observable<string[]> {
    return this.http.get( this.preferences.ApiConfig.geocoding+"&lat="+lat+"&lon="+lon)
    .map( response => {
      const resp = response["display_name"].toString().replace('"','')
      const address = resp.split(',')
      return address
    })
  }

}
